Module: Commerce Emporiki Bank
Author: Yannis Karampelas - yannisc - netstudio.gr


Description
===========
Adds a payment method to Drupal Commerce to accept credit card payments through
the Emporiki Bank (Greek bank) API via XML (not redirection).

Provides options to select the live or the test Emporiki Bank environment and also to
have money authorized only or authorized and captured.

Works with euros, dollars and pounds. If there is need for more currencies,
please contact me sending me the currency code and I'll add them.


Requirements
============

* Commerce



Installation
============

* Copy the 'commerce_emporiki' module directory in to your Drupal
  sites/all/modules directory as usual.

* Enable the module from the Modules > Commerce Payments section


Setup
=====

* Go to Commerce Admin Menu > Payment Methods and enable Emporiki API.

* Edit Emporiki API and select whether you want to operate in the test or the
  live environment, input your merchant id, username and password (provided by Emporiki Bank).


Sponsor
=======
This project is created and maintained by netstudio, a Drupal E-Commerce Solutions company in Athens, Greece.

History
=======

First release: 2/2/2012
